import axios from 'axios'

const isMockData = true
const mockData = {
  srpframeConfigContent: '{"dashboard":{"url":"https://dashboard-dashboard-eks001.hz.wise-paas.com.cn","westUrl":"https://dashboard-dashboard-eks001.hz.wise-paas.com.cn/d/QZg-PWmGz/west","eastUrl":"https://dashboard-dashboard-eks001.hz.wise-paas.com.cn/d/kN4EPWiGk/east"},"title":{"description":"Intelligent City & Factory Management","westTitle":"Building Energy KPI","eastTitle":"Realtime Energy Usage"},"frame":{"main":{},"west":{"isGrafana":true,"addTitle":true,"slideshow":false,"slideshowInterval":"30s","urlList":[{"url":"https://dashboard-dashboard-eks001.hz.wise-paas.com.cn/d/QZg-PWmGz/west"}]},"east":{"isGrafana":true,"addTitle":true,"slideshow":false,"slideshowInterval":"30s","urlList":[{"url":"https://dashboard-dashboard-eks001.hz.wise-paas.com.cn/d/kN4EPWiGk/east"}]}},"menu":{"slideshowInterval":"10s","list":[{"title":"Overview","id":"m2i_EMS1","type": "url","url":"https://sc-ken-saascomposer-eks008.sa.wise-paas.com/sceneAndDisplay.html?org_id=7&tag=scenes/3期.json&allCacheTime=1601283337335&fullscreen=1"},{"title":"Video surveillance","id":"m2i_EMS2","type":"url","url":"https://sc-ken-saascomposer-eks008.sa.wise-paas.com/sceneAndDisplay.html?org_id=7&tag=scenes/copy.json&allCacheTime=1601284341772&fullscreen=1"},{"title":"Entry Access Control","id":"3","type":"url","url":"https://sc-ken-saascomposer-eks008.sa.wise-paas.com/sceneAndDisplay.html?org_id=7&tag=displays/人員門禁.json&allCacheTime=1601284665257&fullscreen=1"}]},"alerting":{"link":"https://dashboard-grafana.wise-paas.com","linkEnable":"false","api":"https://api-test-srpframe.wise-paas.com/test","webSocket":"wss://portal-rmm-develop.wise-paas.com/event/-1","visible":true,"accumulative":false},"notice":{"link":"https://dashboard-grafana.wise-paas.com","linkEnable":"true","api":"https://api-test-srpframe.wise-paas.com/test2","webSocket":"wss://portal-rmm-develop.wise-paas.com/event/-1","visible":true,"accumulative":false},"marquee":{"enable":"true","url":"https://api-test-srpframe.wise-paas.com/marqueetest","refresh":"never","text":"SRPFrame Demo"},"login":{"visible":true},"refreshAlertNotice":"1m"}'
}

function getSrpframeConfigContent (param) {
  const p = new Promise((resolve, reject) => {
    if (isMockData) {
      resolve({ content: mockData.srpframeConfigContent })
    }
    const params = {
      url: `/api/project/monitor/srpframe/config/lang?monitor_id=${param}&timestamp=${new Date().getTime()}`,
      method: 'GET',
      timeout: {
        client: 10 * 1000
      }
    }
    axios(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

export default {
  getSrpframeConfigContent
}
